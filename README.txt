CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Preparation
 * Installation


INTRODUCTION
------------

Current Maintainer: Pixelpiloten <pixelpiloten@gmail.com>

A module that is using the new Twitter api (v1.1, using OAUTH) to fetch and
store a selected nr of tweets on every cron-run so if the api changes or can't
be reached we still can display the latest tweets it fetched.


FEATURES
--------

* Creates a configuration-page where you can add your twitter-details and how
  many tweets you want to list.

* Creates a block where the tweets are listed.

* Creates a permission for deciding what user-roles can change the modules
  settings.


PREPARATION
-----------

To use this module you need to have an Twitter OAUTH-API key, which you can get here
https://dev.twitter.com/apps/new, when you have generated this key, copy the
values somewhere you can remember them so we can use them later.


INSTALLATION
------------

1. Activate the module as usual.

2. Go to the configuration-page admin/structure/twittercache.

4. Input your twitter-handle, and number of tweets you want to list in your
   block.

5. Input the Twitter OAUTH-details, and save.

6. If you now have inputted the right details you should get your new tweets at
   every cron, if you want to fetch the latest now, you can click on the button:
   "Fetch tweets" and it will fetch the latest.
