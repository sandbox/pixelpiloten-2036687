<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if ($block->subject): ?>
    <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
  <?php endif;?>
  <?php print render($title_suffix); ?>

  <?php foreach ($tweets as $tweet): ?>
    <div class="row">
      <div class="tweetinfo">
        <span class="name"><?php print $tweet->user->name; ?></span> <a href="http://www.twitter.com/<?php print $tweet->user->screen_name; ?>" target="_blank">@<?php print $tweet->user->screen_name; ?></a>
      </div>
      <div class="tweet">
        <?php print $tweet->text; ?>
      </div>
      <div class="tweetinfo"><?php print twittercache_ago(strtotime($tweet->created_at)); ?></div>
    </div>
  <?php endforeach; ?>
  <a href="https://twitter.com/intent/follow?screen_name=<?php print $tweet->user->screen_name; ?>" class="follow_us">Follow us on twitter</a>

</div>
